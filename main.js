class Base {
    constructor() {
    }

    shuffleArray(array) {
        array.sort(() => Math.random() - 0.5);
        array.sort(() => Math.random() - 0.5);
    }

    addEvent(el, type, fn) {
        if (el && el.nodeName || el === window) {
            el.addEventListener(type, fn, false);
        } else if (el && el.length) {
            for (var i = 0; i < el.length; i++) {
                this.addEvent(el[i], type, fn);
            }
        }
    }

    removeClassFromEle(ele, _class) {
        if(ele.classList.contains(_class)) {
            ele.classList.remove(_class);
        }
    }

    addClassToEle(ele, _class) {
        if(!ele.classList.contains(_class)) {
            ele.classList.add(_class);
        }
    }
}

class DomElements {
    constructor() {
        this.templates = document.getElementsByTagName("template");
    }

    getDrawItemClone() {
        return this.templates[0].content.cloneNode(true);
    }
}

const _domElements = new DomElements();
Object.freeze(_domElements);

const _conData = new TaskData();
Object.freeze(_conData);

class Main extends Base{
	constructor(pocetPrvku = 9) {
	    super();
        this._pocet = pocetPrvku;
		this._Data = new TaskData();
        this._templates = document.getElementsByTagName("template");
        this._dropItems = [];
        this.init();
        console.log(this.dropItems, this.Data);
        this._oEval = new Evaluation(this.dropItems, this.Data);
        this.setEvents();
	}

	get Data() {
	    return this._Data;
    }

    get templates() {
	    return this._templates;
    }

    get pocet() {
	    return this._pocet;
    }

    get dropItems() {
	    return this._dropItems;
    }
    get oEval() {
	    return this._oEval;
    }

	init() {
        /// naplnit dragItems ///
        var con1 = document.getElementById('kont_kos');
        let arr = Array.from(Array(this.pocet).keys());
        this.shuffleArray(arr);
        let shuffRespA = [...this.Data.respA];
        shuffRespA.sort();
        arr.forEach(i => {
            //console.log(i, this.Data.respA[i]);
            let item = new Item(i + 1,
                [this.Data.qImg[i], /// img
                [shuffRespA, [this.Data.respA[i]]], /// nabidka A, odp. A
                [[...this.Data.respB_options[i]], this.Data.respB_answers[i]], /// nabidka B, odp. B
            ]);
            con1.appendChild(item.domEle);
            this.dropItems.push(item);
        })
        let divClear = document.createElement('div');
        divClear.classList.add('clear');
        con1.appendChild(divClear);
    }

    setEvents() {
        var dropTarg = document.querySelectorAll('div.target_area, #kont_kos');
	    /// nastavit DROP oblasti ///
        this.addEvent(dropTarg, 'dragenter', evt => this.enter(evt));
        this.addEvent(dropTarg, 'dragover', evt => this.cancel(evt));
        this.addEvent(dropTarg, 'dragleave', evt => this.leave(evt));
        this.addEvent(dropTarg, 'drop', e => {
            //console.log("drop area", e.target, e.dataTransfer.getData('Text'))
            if (e.preventDefault) e.preventDefault(); // stops the browser from redirecting off to the text.

            /// zajisteni, ze se Item dropuje ve spravne strukture ///
            var dropTarget = null;
            for(let i = 0; i < dropTarg.length; i++) {
                if (e.target == dropTarg[i]) {
                    dropTarget = e.target;
                    break;
                }
            }
            if(!dropTarget && e.target.parentElement.parentElement.id == "kont_kos") {
                dropTarget = e.target.parentElement.parentElement;
            }

            /// pokud se dropuje mimo povolene oblasti, tak se nedovoli ///
            if(!dropTarget) {
                return false
            }
            /// remove over class from kont ///
            if(dropTarget.classList.contains('over')) {
                dropTarget.classList.remove("over");
            }
            //console.log("drop area", dropTarget, e.dataTransfer)
            var data = document.getElementById(e.dataTransfer.getData('Text'));
            dropTarget.appendChild(data);

            /// zajisteni clear za floatovanim ///
            var cl = dropTarget.querySelector('div.clear');
            if(cl == null) {
                cl = document.createElement('div');
                cl.classList.add("clear");
            }
            dropTarget.appendChild(cl);
            return false;
        });

        this.addEvent(document.getElementById('checkButton'), 'click', (evt) => this.oEval.evaluate(evt));
	}

    cancel(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        return false;
    }

    leave(e) {
        this.removeClassFromEle(e.target,'over');
        return this.cancel(e);
    }

    enter(e) {
        /// target area
        if(e.target.classList.contains('over') == false) {
            e.target.classList.add("over");
        }
        return this.cancel(e);
    }

}

class Evaluation {

    constructor(items, data) {
        //console.log('Evaluation: ', items, data);
        if(items instanceof Object) {
            this._items = items;
        }else {
            this._items = [];
        }

        /// array of ids numbers 1,2 ....///
        this._Data = data;
        this._targetEles = [];
        data.targetsIds.forEach(num => {this._targetEles.push(document.getElementById('target'+num))});
    }

    get items() {
        return this._items;
    }

    get targets() {
        return this._targetEles;
    }

    get Data() {
        return this._Data;
    }

    removeHodnoceni() {
        let respEle = document.querySelectorAll('select.rA, span.popup-switch, div.drag.in, div.target_area');
        respEle.forEach(ele => {
            ele.classList.remove('rError');
            ele.classList.remove('rOk');
            ele.classList.remove('rNotOk');
        })
    }

    checkItemInnerAnswers(item) {
        let respA = item.domEle.querySelector('select.rA');
        if(item.aRespA_ok.find(element => element == respA.value) != undefined) {
            respA.classList.add('rOk');
        }else {
            respA.classList.add('rError');
        }

        //console.log(respA.value)
        var inputs = item.domEle.querySelectorAll('.rB input:checked');
        var errorB = false;
        for(let i = 0; i < inputs.length; i++) {
            if(item.aRespB_ok.find(element => element == inputs[i].value) == undefined) {
                item.domEle.querySelector('span.popup-switch').classList.add('rError');
                errorB = true;
                break;
            }
        }
        /// nejsou vsechny odpovedi spravne ///
        if(!errorB && item.aRespB_ok.length > inputs.length) {
            item.domEle.querySelector('span.popup-switch').classList.add('rNotOk');
            errorB = true;
        }
        if(!errorB) {
            item.domEle.querySelector('span.popup-switch').classList.add('rOk');
        }
    }
    evaluate(e) {
        this.removeHodnoceni();
        /// označit prázdné target frames ///
        this.targets.forEach(ele => {
            if(!ele.querySelector('div.drag.in')) {
                ele.classList.add('rError');
            }
        })

        /// zobrazit legendu ///
        let legend = document.getElementById('checkLegend');
        if(!legend.classList.contains('visible')) {
            legend.classList.add('visible');
        }

        /// projít veškeré drag items ///
        this.items.forEach(item => {
            let clList = item.domEle.parentElement.classList;
            if(item.domEle.parentElement.id == 'target' + item.id) {
                item.domEle.parentElement.classList.add('rOk');
                this.checkItemInnerAnswers(item);
            }else if(item.domEle.parentElement.classList && item.domEle.parentElement.classList.contains('target_area')) {
                item.domEle.parentElement.classList.add('rError');
                this.checkItemInnerAnswers(item);
            }
            //console.log(item.id, item.domEle.parentElement);
        })
    }
}

document.onreadystatechange = function () {
		if (document.readyState == "complete") {  
        	new Main();
    	}
}
