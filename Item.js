class Item extends Base {
    constructor(id, data) {
        super();
        this._id = id;
        this._imgSrc = 'img/' + data[0];
        this._aRespA = data[1][0];
        //this.shuffleArray(this._aRespA);
        this._aRespA_ok = data[1][1];
        this._aRespB = data[2][0];
        this.shuffleArray(this._aRespB);
        this._aRespB_ok = data[2][1];

        this.touch = {'longpress': false, 'presstimer': null, 'showTarget' : null}


        this._domEle = _domElements.getDrawItemClone().querySelector('#q-img-');
        this._domEle.id = '#q-img-' + id;
        this._domEle.querySelectorAll('img')[0].src = this._imgSrc;

        this.setRespA();
        this.setRespB();

        /// nastavit klikaci POPUP ///
        this.addEvent(this._domEle.querySelector('span'), 'click', evt => this.listenPopupSelect(evt));


        /*this.addEvent(this._domEle, "mousedown", this.startTiming.bind(this));
        this.addEvent(this._domEle, "mouseout", this.cancelTiming.bind(this));
        */
        /*this.addEvent(this._domEle, "touchstart", this.startTiming.bind(this));
        this.addEvent(this._domEle, "touchend", this.cancelTiming.bind(this));
        this.addEvent(this._domEle, "touchleave", this.cancelTiming.bind(this));
        this.addEvent(this._domEle, "touchcancel", this.cancelTiming.bind(this));
        */
        this.addEvent(this._domEle, "click", this.clickTiming.bind(this));
        //this.addEvent(this._domEle, "contextmenu", (evt) => this.clickTiming(evt));

        this.addEvent(this._domEle, 'dragstart', evt => this.listenDragItem(evt));
    }

    get id() {
        return this._id;
    }

    get aRespA_ok() {
        return this._aRespA_ok;
    }

    get aRespB_ok() {
        return this._aRespB_ok;
    }

    get domEle() {
        return this._domEle
    }

    clickTiming(e) {
        //console.log("click", e.type, e.target);
        /*if (this.touch.presstimer !== null) {
            clearTimeout(this.touch.presstimer);
            this.touch.presstimer = null;
        }
        /// remove class for click
        e.target.classList.remove("press");
        if (this.touch.longpress) {
            e.preventDefault();
            e.target.classList.remove("longpress");
            return false;
        }
        */
        if (e.type == 'click' && (e.target == this.domEle || e.target == this.touch.showTarget)) {
            if(this.touch.showTarget) {
                this._domEle.removeChild(this.touch.showTarget)
                this.touch.showTarget = null;
            }else {
                this.touch.showTarget = this._domEle.querySelector('img').cloneNode(true);
                this.touch.showTarget.classList.add('topImg');
                this._domEle.appendChild(this.touch.showTarget);
            }
            //alert("press");
        }

        if (e.type == 'contextmenu') {
            //e.preventDefault();
            //alert('klikni levym')
            //var ev = document.createEvent('HTMLEvents');
            //ev.initEvent('contextmenu', true, false);
            //e.target.querySelector('img').dispatchEvent(ev);
            //return false;
        }
    }
    cancelTiming(e) {
        //console.log("cancel", e.type);
        if (this.touch.presstimer !== null) {
            clearTimeout(this.touch.presstimer);
            this.touch.presstimer = null;
            //console.log("TP", 'remove longpress');
            //alert("remove longpress" + "\n" + e.type);
        }
        e.target.classList.remove("longpress");
        e.target.classList.remove("press");

    }

    startTiming(e) {
        //console.log("start", e.type);

        if (e.type === "click" && e.button !== 0) {
            return;
        }
        this.touch.longpress = false;

        e.target.classList.add("press");
        //console.log("TP", 'add press');

        this.touch.presstimer = setTimeout(() => {
            //console.log("TP", "add longpress");
            e.target.classList.add("longpress");
            //alert("add longpress" + "\n" + e.type);
            this.touch.longpress = true;
        }, 1000);

        return false;
    }

    setRespA() {
        var target = this._domEle.querySelector('select.rA');
        var opt = document.createElement('option');
        opt.innerText = '-vyber';
        target.appendChild(opt.cloneNode(true));
        for (let i = 0; i < this._aRespA.length; i++) {
            let o = opt.cloneNode()
            o.innerText = this._aRespA[i];
            target.appendChild(o);
        }
    }

    setRespB() {
        var target = this._domEle.querySelector('.rB');
        var el = document.createElement('label');
        var elIn = document.createElement('input');
        elIn.setAttribute('type','checkbox');
        for (let j = 0; j < this._aRespB.length; j++) {
            let lab = el.cloneNode();
            let inp = elIn.cloneNode();
            inp.setAttribute('id', 'rB-'+this._id+'-'+j);
            inp.setAttribute('value', this._aRespB[j]);
            this.addEvent(inp, 'click', (e) => {
                //console.log("input", e.target)
                if(e.target.checked) {
                    this.addClassToEle(e.target.parentNode, 'checked');
                    this.addClassToEle(e.target, 'checked');
                }else {
                    this.removeClassFromEle(e.target.parentNode, 'checked');
                    this.removeClassFromEle(e.target, 'checked');
                }
            });
            lab.appendChild(inp);
            lab.appendChild(document.createTextNode(_conData.respB[this._aRespB[j]]));
            target.appendChild(lab);
        }
    }

    listenDragItem(e) {
       e.dataTransfer.setData('Text', e.target.id);
       //console.log(this._id, e.target)
    }

    listenPopupSelect(e) {
        //console.log(e.target.parentNode.querySelector('div.popup-select'))
        //console.log(e.target)
        if(e.target.classList.contains('on')) {
            this.removeClassFromEle(e.target, 'on')
            e.target.parentNode.querySelector('div.popup-select').style.display = 'none'
        }else {
            this.addClassToEle(e.target, 'on')
            e.target.parentNode.querySelector('div.popup-select').style.display = 'block'
        }
    }

    checkRespA(value) {
        return true;
    }

    checkRespB(value) {
        return true;
    }

    setErrorA() {

    }

    setErrorB() {

    }

    removeErrors() {

    }
}