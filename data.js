class TaskData {
    constructor() {
      this.qImg = ['q1.jpg', 'q2.jpg', 'q8.jpg', 'q4.jpg', 'q6.jpg', 'q9.jpg', 'q3.jpg', 'q5.jpg', 'q7.jpg',];
      this.respA = ['antika', 'byzanc', 'románský sloh', 'gotika', 'renesance', 'baroko', 'empír/klasicismus', 'secese', 'kubismus'];
      this.respB = ['dórský sloh','Akropole','minaret','iónský sloh','lomený oblouk','korintský sloh','zdobené portály','francouzské parky','voluta','květinové ornamenty','mozaika z kamínků','kopule','fresky','hranaté okno','Hagia Sofia','Ravena','sgrafita','rotunda','bazilika','sdružená okna','půlkruhový oblouk','rozeta','křížová klenba','opěrný systém','Notre Dame','hranatá okna','zámky','omítka','Kačina','Schwarzenberský palác','zdobená průčelí','kontrast barev a materiálů','zdobení zlatem','iónské a korintské hlavice sloupů',' Kačina','geometrické tvary','inspirace antikou','křivka a linie','inspirace v historii','česká avantgarda','Dům U Černé Matky Boží','užitečnost','perspektiva'];
      this.respB_options = [
          [0,1,2,3,4,5,6,7,8,9],
          [10,3,11,8,12,13,14,15,16],
          [17,18,4,19,8,20,9,6,1],
          [4, 8, 21, 22, 14, 23, 0, 24, 19, 9],
          [16, 10, 25, 4, 26, 27, 28, 29, 22, 17],
          [30, 16, 8, 11, 20, 31, 21, 32, 19, 17],
          [33, 18, 28, 1, 35, 36, 21, 7, 4, 16],
          [32, 18, 9, 19, 10, 37, 16, 38, 0, 24],
          [39, 32, 35, 22, 40, 41, 18, 42, 36, 21]
          
      ];
      this.respB_answers = [
          [0,1,3,5,8],
          [10,11,12,14,15],
          [17,18,19,20,6],
          [4, 21, 22, 23, 24],
          [16, 25, 26, 27, 29],
          [30, 8, 11, 31, 32],
          [33, 28, 35, 36, 7],
          [32, 9, 10, 37, 38],
          [39, 35, 40, 41, 42]
      ];

      this.targetsIds = [1,2,3,4,5,6,7,8,9];
    }

}

/*
{
  "slohy": ['antika', 'byzanc', 'románský sloh', 'gotika', 'renesance', 'baroko', 'empír/klasicismus', 'secese', 'kubismus'],
  "znaky": ['dórský sloh','Akropole','minaret','iónský sloh','lomený oblouk','korintský sloh','zdobené portály','francouzské parky','voluta','květinové ornamenty','mozaika z kamínků','kopule','fresky','hranaté okno','Hagia Sofia','Ravena','sgrafita','rotunda','bazilika','sdružená okna','půlkruhový oblouk','rozeta','křížová klenba','opěrný systém','Notre Dame','hranatá okna','zámky','omítka','Kačina','Schwarzenberský palác','zdobená průčelí','kontrast barev a materiálů','zdobení zlatem','iónské a korintské hlavice sloupů',' Kačina','geometrické tvary','inspirace antikou','křivka a linie','inspirace v historii','česká avantgarda','Dům U Černé Matky Boží','užitečnost','perspektiva'],
  "znaky1": [0,1,2,3,4,5,6,7,8,9],
  "znaky2": [10,3,11,8,12,13,14,15,16],
  "znaky3": [],
  "znaky4": [],
  "znaky5": [],
  "znaky6": [],
  "znaky7": [],
  "znaky8": [],
  "znaky9": [],
  "spravne1": [],
  "spravne2": [],
  "spravne3": [],
  "spravne4": [],
  "spravne5": [],
  "spravne6": [],
  "spravne7": [],
  "spravne8": [],
  "spravne9": []
}
*/